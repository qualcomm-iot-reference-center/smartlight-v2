//Pino que responsável pela excitação do Triac
#define triac 2
//Variável utilizada para controlar a potência da lâmpada, variando de 0 a 100 (%).
volatile int power = 0;

// Variável utilizada para receber valores enviados de forma serial, estes podem ser "+", "-", "l", "d"
char leitura;


void setup()
{
    //Configuro o pino do triac como saída
     pinMode(triac, OUTPUT);
     //Configuro o pino 2 como entrada e com resistor de PULLUP. Este será o pino responsável por identificar quando a fase senoidal ser igual a zero, isto é, v(t) = 0
     pinMode(3,INPUT_PULLUP);
     //Inicializo interrupção zero_cross que será chamada toda vez que v(t) = 0 
     //1º Parâmetro se refere ao pino que irá ser monitorado para realizar a chamada da interrupção
     //2º Parâmetro se refere ao nome da função que irei chamar quando a interrupção ocorrer (A função chamada não recebe valores e não retornada nada)
     //3º Parâmetro se refere a quando a interrupção será acionada, neste caso em particular, no momento em que o pino 2, passar do valor 1 para 0
     attachInterrupt(1, zero_cross, FALLING);
     //Configuro a velocidade da comunicação serial, no caso 9600bps
     Serial.begin(9600);
}

void loop()
{
     //Verifico se há disponibilidade de comunicação Serial
     if(Serial.available())
     {
          //Recebo um char e armazeno na variável leitura
          leitura = Serial.read();
     }
   
     //Valido se a variável leitura é "-"
     if(leitura == '-')
     {
          //Se se power for maior que 0%, realizo um decremento desta de 5%
          if(power > 0)
          {
               power = power - 5;
          }
       
          delay(200);
          //Imprimo serialmente o valor de power + o texto "%"
          Serial.print(power); 
          Serial.println(" %"); 
     }

     //Valido se a variável leitura é "+"
     if(leitura == '+')
     {
          //Se se power for menor que 100%, realizo um incremento desta de 5%
          if(power < 100)
          {
               power = power + 5;
          }
       
          delay(200); 
          //Imprimo serialmente o valor de power + o texto "%"
          Serial.print(power); 
          Serial.println(" %"); 
    }

     //Valido se a variável leitura é "d"
     if(leitura == 'd')
     {
          //Power é definido como 0%, portanto efetuo o desligamento da carga
          power = 0;    
          //Imprimo serialmente o valor de power + o texto "%"
          Serial.print(power); 
          Serial.println(" %"); 
     }

   
     //Valido se a variável leitura é "l"
     if(leitura == 'l')
     {    
           //Power é definido como 100%, portanto ligo a carga com potência máxima (50% da potência no caso da luminária 220)
           power = 100;    
           //Imprimo serialmente o valor de power + o texto "%"
           Serial.print(power); 
           Serial.println(" %"); 
     }

     if(leitura == 'm')
     {
	   power = 40;
           Serial.print(power);
           Serial.println("%");
     }

     if(leitura == 'h')
     {
           power = 70;
           Serial.print(power);
           Serial.println("%");
     }

     leitura = ' '; //Atribuo o valor espaço (' ') para a variável leitura, garantindo que no próximo loop qualquer alteração de power só será efetuada mediante ação do usuário
}

//Função executada quando é detectado a passagem por zero
void zero_cross()
{
     // A rede tem uma frequência de 60Hz, o que resulta em um período de 16,67ms.
     // Como nos interessa o controle tanto no semi-ciclo negativo quanto no positivo, realizo a divisão por 2. => 16,67 / 2 = 8,33ms
     // Portanto para o cálculo do ângulo de disparo temos a seguinte conta: 8,33ms / 100 partes, onde cada parte será 1% = 83,33us.
     // Logo a constante de multiplicação é aproximadamente 83us, nos possibilitando assim trabalhar em condução e em corte com o triac, em qualquer parte de um semi-ciclo da fase
     
     // Através do cálculo acima, consigo criar uma variável que irá receber a quantidade de tempo que o TRIAC deve permanecer em corte, em us
     // O powertime é o tempo em us, que o TRIAC permanecerá em corte
     int powertime = (83*(100-power));

     // Caso a potência seja próxima de 100 
     // No caso de 100% do power, com um powertimer muito baixo, se conduz logo após a detecção, ligando a lâmpada com potência máxima
     if(powertime <= 300)
     {
          // Aciono o triac para que este passe a conduzir
          digitalWrite(triac, HIGH);
     }
   
     // No caso de 0% do power, com um powertimer muito baixo, se conduz logo após a detecção, ligando a lâmpada com potência máxima
     else if(powertime >= 8000)
     {
          // Não mando sinal para o triac, para que este não conduza
          digitalWrite(triac, LOW);
     }
   
     //Nos casos de power entre 5 a 95%, efetuo um controle preciso, deixando o triac em corte por powertime, conduzindo após este tempo
     else if((powertime > 300) && (powertime < 8000))
     {
          //Mantém o triac em corte por powertime microssegundos.
          delayMicroseconds(powertime);
       
          // Excito o triac para que este passe a conduzir
          digitalWrite(triac, HIGH);
       
          // 8,33 us de delay para que o TRIAC perceba o pulso
          delayMicroseconds(8.33);
 
          //Desliga a excitação do triac, contanto que a corrente a circular pelo triac atenda um valor minimo, este continua a conduzir até a fase ser de valor nulo v(t) = 0
          digitalWrite(triac, LOW);
     }
}