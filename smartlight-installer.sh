#! /bin/bash
#!/usr/bin/expect -f

echo '--- AS INSTALAÇÕES NECESSÁRIAS SERÃO EXECUTADAS ---'

echo '--- ATUALIZANDO/INSTALANDO OS PACOTES/PROGRAMAS DISPONÍVEIS ---'
sudo iwconfig wlan0 power off
sudo apt-get update

echo '--- ATUALIZANDO O SISTEMA ---'
sudo apt-mark hold linux-image-4.14.0-qcomlt-arm64
sudo apt-get dist-upgrade

echo '--- INSTALANDO BIBLIOTECAS E FRAMEWORKS NECESSÁRIAS ---'
sudo apt-get install python-pip -y 
sudo apt-get install python-opencv -y 
sudo pip install gtts
sudo apt-get install mpg321 -y
sudo pip install imutils
sudo pip install pyserial

echo '--- INSTALANDO BIBLIOTECAS DO ARDUINO PARA CONTROLE DA LUMINÁRIA ---'
sudo apt-get install build-essential cmake pkg-config -y
sudo apt-get install arduino-mk arduino git build-essential autoconf libtool swig3.0 -y
sudo apt-get install python-dev nodejs-dev cmake pkg-config libpcre3-dev gedit aptitude v4l-utils -y
sudo apt-get clean
cd ~/Downloads
sudo git clone http://github.com/intel-iot-devkit/mraa
cd mraa
sudo mkdir build
cd build
sudo cmake -DBUILDSWIGNODE=OFF ..
sudo make
sudo make install
sudo ldconfig /usr/local/lib
sudo adduser linaro i2c
cd ~/Downloads
sudo git clone http://github.com/96boards/96boards-tools
sudo cp 96boards-tools/70-96boards-common.rules /etc/udev/rules.d/
sudo echo "export PYTHONPATH="$PYTHONPATH:/usr/local/lib/python2.7/site-packages"" >> /etc/udev/rules.d/70-96boards-common.rules

# Instalação de bluetooth

echo '--- Configurar dispositivo bluetooth? (Opcional) ---'
echo -n '[y/n]:'
read option1

if [ "${option1,}" = "y" ] || [ "${option1,}" = "Y" ]
then
	echo '--- Digite o MAC Address do dispositivo: ---'
	echo -n ':'
	read MAC
	sudo expect /home/linaro/Downloads/bluetooth.sh "$MAC"
fi

echo '--- SMARTLIGHTV2 INSTALADO ---'
echo '** Configure a variável path no arquivo V2Smart_light.py com o caminho absoluto até a pasta rois. **'
echo '** Após configurá-la, execute: sudo python V2Smart_light.py **'

