#!/usr/bin/expect -f
# Conecta o bluetooth, precisa passar o MAC adress como parametro

set prompt '#'
set adress [lindex $argv 0]

spawn sudo bluetoothctl
send "power on\r"
send "agent on\r"
send "default-agent\r"
send "trust $adress\r"
sleep 2
send "pair $adress\r"
sleep 3
send "connect $adress\r"
sleep 4
send "quit\r"
expect eof