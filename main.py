import cv2
import argparse
import datetime
import imutils
import os
import serial
import time
import thread
import shlex
import subprocess

# path to save images
path = '/home/linaro/Desktop/smartlight-v2/rois/'

# Creating argument parse: Camera index
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--camera", type=int, default=0,
                help="camera index (0, 1, 2, ...)")
ap.add_argument("-w", "--win-stride", type=int, default=(8, 8),
                help="window stride")
ap.add_argument("-p", "--padding", type=int, default=(4, 4),
                help="object padding")
ap.add_argument("-s", "--scale", type=float, default=1.05,
                help="image pyramid scale")
ap.add_argument("-r", "--reason", type=float, default=0.045,
                help="minimum reason between contour area and text area to "
                     "the centroid be considered a person")
ap.add_argument("-d", "--detection", type=int, default=10,
                help="amount of frames to wait until verify if there is"
                     "someone new in the image")
ap.add_argument("-v", "--verify", type=int, default=30,
                help="amount of frames until the next verification of "
                     "ROIs is executed")

def speech():
	os.system("mpg321 -q welcome.mp3")

op = None
arduinoPort = serial.Serial("/dev/tty96B0", 9600)

args = vars(ap.parse_args())

def python_to_bash(comando_bash):
    args = shlex.split(comando_bash)
    comando = subprocess.Popen(args, stdout=subprocess.PIPE)
    saida, erro = comando.communicate()
    return saida.decode('utf-8'), erro

lista_dispositivos = 'v4l2-ctl --list-devices'
saida, erro = python_to_bash(lista_dispositivos)
lista_saida = saida.split('\n')

for n_linha, texto in enumerate(lista_saida):
    if 'usb' in texto:
        nome_camera = lista_saida[n_linha + 1].strip()
        break

# Create a VideoCapture object and read from the camera
cam = cv2.VideoCapture(int(nome_camera[-1]))

# Check if camera opened successfully and
if cam.isOpened() is False:
    print('Failed to connect with the camera')
else:

    # Creates the HOG descriptor and set its SVM detector type to people detection
    hogdef = cv2.HOGDescriptor()
    pdetect = cv2.HOGDescriptor_getDefaultPeopleDetector()
    hogdef.setSVMDetector(pdetect)
    centroids = []
    index = []
    ctradius = args["reason"]
    verify = args["verify"]
    qt = 0  # Quantity of detected people (for SmartLight)
    detected = 0  # Count number of frames in which a person hasn't been detected
    fPerson = 0  # Flags the first person detected
    count = 0  # ROI indexing
    yfind = 1  # Centroid flag
    nfind = 0  # Centroid flag

    rois = len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])

    if rois > 0:
        count = rois

    # Read until camera is open or 'q' is pressed
    while cam.isOpened():
        ret, frame = cam.read()
        height, width, channels = frame.shape
        area = height * width

        # Checks if the image was successfully read
        if ret is True:
            # Detect people in the image
            frame = imutils.resize(frame, width=min(400, frame.shape[1]))
            start = datetime.datetime.now()
            (rects, weights) = hogdef.detectMultiScale(frame, winStride=args["win_stride"], padding=args["padding"], scale=args["scale"])
            if len(rects) != 0:
                detected = detected + 1
                timed = (datetime.datetime.now() - start).total_seconds()

            verify = verify + 1

            # Draw the bounding boxes
            for (x, y, w, h) in rects:
                timeString = "{}:{}:{} - {}/{}/{}".format(start.hour, start.minute, start.second,
                                                    start.month, start.day, start.year)

                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

                # Find centroid
                roi = frame[y:y+h, x:x+w]
                gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                blurred = cv2.GaussianBlur(gray, (5, 5), 0)
                thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

                img2, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)

                # Loop over the contours
                for c in cnts:
                    cArea = cv2.contourArea(c)
                    # print(cArea/area)
                    if cArea / area > ctradius:
                        # compute the center of the contour
                        M = cv2.moments(c)
                        if M["m00"] != 0:
                            cX = int(M["m10"] / M["m00"])
                            cY = int(M["m01"] / M["m00"])
                            # Storing centroid and picture of the first person
                            if fPerson is 0 and (cX > 0 and cY > 0):
                                centroids.append([cX + x, cY + y])
                                yTxt = y + 10
                                xTxt = x
                                cv2.putText(roi, timeString, (xTxt, yTxt), cv2.FONT_HERSHEY_COMPLEX, 0.25, (0, 0, 255))
                                text = "roi" + str(count) + ".png"
                                cv2.imwrite(os.path.join(path, text), roi)
                                print("Someone entered the room!")
                                print("Giving a warming welcome...")
                                thread.start_new_thread(speech, ())
                                print("Detection took: {}s".format(timed))
                                print(text + " saved")
                                print("People in the environment: {}".format(len(centroids)))
                                count = count + 1
                                fPerson = fPerson + 1
                                qt = qt + 1

                # If someone has been detected, verify if the new centroid is in some ROI
                if verify % 10 == 0:
                    if len(centroids) > 0:
                        nfind = 0
                        for i in range(len(centroids)):
                            for (x, y, w, h) in rects:
                                    if centroids[i][0] > 0 and centroids[i][1] > 0:  # Verify if the centroid is valid
                                        if centroids[i][0] is not cX and centroids[i][1] is not cY:  # If the centroids aren't equal
                                            if ((centroids[i][0] >= x and centroids[i][0] <= x + w) and (
                                                    centroids[i][1] >= y and centroids[i][1] <= y + h)):
                                                nfind = 1  # The stored centroid is in some ROI

                        #  If centroid isn't found at any ROI
                        if nfind == 0:  # Detected a new person, saving its centroid and cropping
                            qt = qt + 1
                            roi = frame[y:y + h, x:x + w]
                            yTxt = y + 10
                            xTxt = x
                            cv2.putText(roi, timeString, (xTxt, yTxt), cv2.FONT_HERSHEY_COMPLEX, 0.25, (0, 0, 255))
                            text = "roi" + str(count) + ".png"
                            cv2.imwrite(os.path.join(path, text), roi)
                            print("Person detected.")
                            print("Detection took: {}s".format(timed))
                            print(text + " saved")

                            centroids.append([cX + x, cY + y])
                            count = count + 1

            if verify >= 30 and len(centroids) > 0:  # Verify centroids after 30 frames
                if len(rects) > 0:  # If some person was detected
                    yfind = 1
                    index = []
                    for i in range(len(centroids)):
                        for (x, y, w, h) in rects:
                                if centroids[i][0] > 0 and centroids[i][1] > 0:  # Verify if the centroid is valid
                                    if ((centroids[i][0] >= x and centroids[i][0] <= x + w) and (
                                            centroids[i][1] >= y and centroids[i][1] <= y + h)):
                                        yfind = 0  # The stored centroid is in some ROI
                        if yfind == 1:
                            index.append(centroids[i])
                    # Remove invalid centroids that not belongs to some ROI
                    for i in range(len(index)):
                        centroids.remove(index[i])
                        qt = qt - 1

                    index = []
                elif detected == 0:  # If after some time no one was detected, clean centroids and reset variables
                    if len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))]) > 100:
                        filelist = [f for f in os.listdir(path)]
                        for f in filelist:
                            os.remove(os.path.join(path, f))
                        count = 0
                    centroids = []
                    fPerson = 0
                    qt = 0
                print("People in the environment: {}".format(len(centroids)))
                verify = 0
                detected = 0
            # Show the output image
            cv2.imshow("Detections", frame)

            # Checks people quantity to turn on/off and adjust the brightness
            if qt == 0 and op != "d" and verify >= 10:
                op = "d"
                arduinoPort.write(op.encode())

            elif qt == 1 and op != "m":
                op = "m"
                arduinoPort.write(op.encode())

            elif qt > 1 and op != "h":
                op = "h"
                arduinoPort.write(op.encode())

            # Pause for the lamp
            time.sleep(0.01)

            rects = ()
            # Wait for a click press and checks if the 'q' was pressed
            # If 'q' was pressed, break the loop
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break

    # Stop the camera connection
    cam.release()
    # Destroy all created windows
    cv2.destroyAllWindows()
