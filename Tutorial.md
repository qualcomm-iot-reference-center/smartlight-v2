# Smart Light


## Instalação de frameworks, bibliotecas e softwares utilizados


**Instale manualmente seguindo esse tutorial ou execute o instalador (como root). Para a segunda opção, siga os seguintes passos:**

Clone o repositório:

git clone https://gitlab.com/qualcomm-iot-reference-center/smartlight-v2.git

Entre na pasta:

cd smartlight-v2

Execute o instalador:

./smartlight-installer


(Se ocorrer um erro execute: chmod 777 smartlight-installer)

------------------------------------------------------------------------


**pip, gerenciador de pacotes do python:**

```sh
sudo apt install python-pip
```

**OpenCV:** [Página do OpenCV](https://opencv.org/)

```sh
sudo apt install python-opencv 
```


**gTTs:**  [Página do gTTS na PyPi](https://pypi.org/project/gTTS/)

```sh
sudo pip install gtts 
```

**mpg321:** [Página do mpg321](http://mpg321.sourceforge.net/)

```sh
sudo apt-get install mpg321 
```

**imutils:** [Página do imutils na PyPi](https://pypi.org/project/imutils/)

```sh
sudo pip install imutils 
```

**PySerial:** [Página do pyserial na PyPi](https://pypi.org/project/pyserial/)

```sh
sudo pip install pyserial 
```

**expect:** [Sobre o expect](https://linux.die.net/man/1/expect)

```sh
sudo apt-get install expect 
```

**Bibliotecas do Arduino**

Essas bibliotecas são necessárias para o acionamento e gerenciamento da iluminação, processo
este realizado pelo Arduino presente na Mezzanine acoplada à DragonBoard.

1. iwconfig wlan0 power off

2. sudo apt-get update

3. sudo apt-get install arduino-mk arduino git build-essential autoconf libtool swig3.0 python-dev nodejs-dev cmake pkg-config libpcre3-dev gedit aptitude v4l-utils

4. sudo apt-get clean

5. cd ~/Downloads

6. git clone http://github.com/intel-iot-devkit/mraa

7. cd mraa

8. mkdir build

9. cd build

10. cmake -DBUILDSWIGNODE=OFF ..

11. make

12. sudo make install

13. sudo ldconfig /usr/local/lib/

14. sudo adduser linaro i2c

15. cd ~/Downloads

16. git clone http://github.com/96boards/96boards-tools

17. sudo cp 96boards-tools/70-96boards-common.rules /etc/udev/rules.d/

18. sudo nano /etc/profile.d/96boards-sensors.sh

Copie ou corrija as informações presentes no arquivo, de tal modo que fiquem
conforme a seguir:

export JAVA_TOOL_OPTIONS="-Dgnu.io.rxtx.SerialPorts=/dev/tty96B0"

export MONITOR_PORT=/dev/tty96B0

export PYTHONPATH="$PYTHONPATH:/usr/local/lib/python2.7/site-packages"

Para salvar as informações: Ctrl + O, Enter e Ctrl + X

19. sudo cp /etc/profile.d/96boards-sensors.sh /etc/X11/Xsession.d/96boards-sensors

20. sudo reboot

 
**Configuração do reprodutor de áudio bluetooth (opcional)** 

Descubra o MAC address do dispositivo, scaneando todos os dispositivos bluetooth do ambiente: 
```sh
sudo bluetoothctl 

scan on 

scan off
```

Confie, pareie e conecte-se ao dispositivo: 

```sh
trust <MAC> 

pair <MAC> 

connect <MAC> 

quit 
```


Configure o crontab para agendar uma tarefa que se conecta ao dispositivo a cada 5 minutos. 

Abra o crontab: 

```sh
crontab -e 
```

Insira o comando no final do arquivo:
```
*/5 **** expect <caminho absoluto do bluetooth.sh> <MAC> 
```

Salve o arquivo (varia conforme o editor). 


## Guia de utilização do projeto

Para fazer o uso do projeto, é necessário seguir os seguintes passos: 

1. Instalar o Python e as dependências do projeto. 
2. Baixar os arquivos do projeto. 
3. Configurar a variável path com o caminho absoluto até a pasta rois, onde serão guardadas as imagens extraídas, dentro da pasta do projeto.
4. Instalar o expect e configurar o dispositivo reprodutor de áudio com conexão bluetooth (opcional) 
5. Executar: 
```sh
sudo python V2Smart_light.py
```


*Lista de argumentos*

* "-c", "--camera", type=int, default=0, help="camera index (0, 1, 2, ...). O valor padrão é zero (opcional).
* "-w", "--win-stride", type=int, default=(8, 8), help="window stride"
* "-p", "--padding", type=int, default=(4, 4), help="object padding"
* "-s", "--scale", type=float, default=1.05, help="image pyramid scale"
* "-m", "--mean-shift", type=int, default=-1, help="whether or not mean shift grouping should be used"
* "-r", "--reason", type=float, default=0.045, help="minimum reason between contour area and text area to the centroid be considered a person"
* "-d", "--detection", type=int, default=10, help="amount of frames to wait until verify if there is someone new in the image"
* "-v", "--verify", type=int, default=30, help="amount of frames until the next verification of ROIs is executed"