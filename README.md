# Smart Light


## Introduction

The SmartLight project intelligently illuminates the environment according to the number of people in it. It was created based on the possibility of generating electricity savings, since it is not necessary for a lamp to be at its maximum power when no one is near.


## Hardware Used

- This application was run on the DragonBoard410c development platform. The operating system image is linaro-buster-alip-dragonboard-410c-359.img.gz and can be obtained in this [link](https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/)

- Kernel version: 

- Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux 

- It should be noted that the system uses an external memory to expand the capacity of the rootfs and provide a swap region. The configuration of this mechanism is as follows: 4GB swap and the remainder dedicated to the rootfs. 

- The luminaire, of conventional model and LED lamps, connected to the 220V electric grid

- A Mezzanine 96Boards Sensors Board shield, for interface between the control circuit and Dragonboard. Mezaninne has an Arduino interface for easy integration.

- A control circuit for the activation of the luminaire, allowing the variation of lighting intensity




### Equipment

Os equipamentos necessários para a montagem da aplicação são:
* DragonBoard 410c;
* 12V Wall adapter;
* USB webcam (a Logitech C920 was used);
* Mezzanine Sensors Board;
* HDMI Monitor;
* micro SD card (16GB);
* USB keyboard;
* Control circuit;
* Generic 220V LED Lamp/Bulb.
  

## Frameworks, libraries and software used

Run the tutorial from this [link](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(pt-br).md) to create a region of Swap memory on the SD card.

At the DragonBoard terminal, run the libraries installation commands:

```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
Sudo apt-get install python3-dev
sudo apt-get install python3-opencv
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module
sudo apt install python3-pip
sudo –H python3 –m pip install scipy
```

## Operation 

**1. Electric circuit**

Next, the operation of the control circuit will be detailed. The circuit has two objectives:

* Mark the drive frequency from the network, used as the input data for the Arduino of the Mezzanine.
* Interface between the control of the software and the luminaire, triggering the shots for power control

 <div align="center">
    <figure>
        <img src="/images/1.PNG">
        <figcaption>Zero Crossing Detector</figcaption>
    </figure>
</div>


First, the 220V signal pass through a 0-pass detection circuit so power control can be done.

The circuit transforms the alternating wave into pulsating, which polarizes the led and triggers the photo transistor. The R2 resistor acts as a pull-up resistor, ie whenever the photo LED transistor is off (0VAC) the Arduino input GPIO will be energized with 5VDC when a pulse occurs and is voltage above 0VAC, the photo transistor triggers and the GPIO Arduino input goes to logic level 0. In this way, it is possible to identify when the sinoidal voltage coming from the network passes from the semicircle positive to the negative or the reverse.

The second part of the circuit consists of the control of luminosity from the arduino. To control the brightness, it is necessary to reduce the power over the lamp, for this a triac with a snubber was used.


 <div align="center">
    <figure>
        <img src="/images/2.PNG">
        <figcaption>AC control circuit</figcaption>
    </figure>
</div>


The output of the arduino is connected to the photo triac, which serves to isolate the output of the microcontroller from the network. The photo triac U3 is connected to the gate of the triac U2, which in turn is responsible for allowing or not allowing the lamp to drive. Depending on the shooting angle above the triac U2, the lamp brightness varies. In this case, the Arduino can synchronize the trips with the network through the zero crossing detection circuit.

 <div align="center">
    <figure>
        <img src="/images/3.PNG">
        <figcaption>Triac output</figcaption>
    </figure>
</div>

**2. Cycle of operation (Software)**

The main method for Smartlight to work is the "cv2.createBackgroundSubtractorMOG2 ()". It is responsible for subtracting the background from the image. After this process, the resulting image is just the outline of a person highlighted on a black background.
Then the person in the image is detected with the "cv2.findContours ()" method. Once the position is detected, the tracking is done in the video, with a tracking technique.

When one or more people are detected, the software sends a serial command to the Mezzanine card, requesting the activation of the control circuit with certain intensity. The circuit lights up the luminaire and awaits a new command.
The software accompanies the movement of people with the trakking, and if they leave the area illuminated, sends a command for the circuit to turn off the luminaire.

## Execution of the project

### Control circuit creation

For execution, you need to create the control circuit board. The schematic diagram of the circuit is shown in the following figure:

 <div align="center">
    <figure>
        <img src="/images/Cirquito Smart Light.JPG">
        <figcaption>Control Circuit</figcaption>
    </figure>
</div>

The image used to print the board:

 <div align="center">
    <figure>
        <img src="/images/Capturar.JPG">
        <figcaption>PCB</figcaption>
    </figure>
</div>

A representation of the board:

 <div align="center">
    <figure>
        <img src="/images/Smart Light.JPG">
        <figcaption>Control Circuit</figcaption>
    </figure>
</div>

The circuit was designed as a shield for the Mezzanine. The Fitting is made in the Arduino pins.

### Software Installation

At the terminal of the board, clone the project repository:

``` sh
git clone https://gitlab.com/qualcomm-iot-reference-center/smartlight-v2.git
cd smartlight-v2

``` 

To run the code, run the command:

``` sh

python3 main_mog.py

```

The application will start. The people count will be displayed on the screen. The lamp should be operated with low intensity if a person is detected by the camera, and in high intensity as the number of people increases.