import cv2 
import numpy as np 
from pyimagesearch.centroidtracker import CentroidTracker


cap = cv2.VideoCapture(0)
fgbg = cv2.createBackgroundSubtractorMOG2()

# initialize our centroid tracker and frame dimensions
ct = CentroidTracker()
(H, W) = (None, None)

rects = []

while True:
# Prepocess
	ret, frame = cap.read()
	frame = frame[100:750, 150:850]
	#fgbg.setShadowValue(0)
	t = fgbg.apply(frame)
	blur = cv2.GaussianBlur(t,(1,1),1000)
	flag, thresh = cv2.threshold(blur, 120, 255, cv2.THRESH_BINARY)
# Find contours
	contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	contours = sorted(contours, key=cv2.contourArea,reverse=False) 
# Show image
	imgcont = frame.copy()
	if len(contours) != 0:

    #find the biggest area
		for i in contours:
			area = cv2.contourArea(i)
			if(area > 10000): #this area maybe need to be changed, this number was referenced to our case.
				x,y,w,h = cv2.boundingRect(i)
				if(h > 100 and w > 100): #In our case, this was the most people average dimensions
					if(w > 400):#when two people walk together
						obj1 = [x, y, x+(w/2), y +h]
						obj2 = [x+(w/2) + 10, y, x + w, y + h]
						rects.append(obj1)
						rects.append(obj2)
					else:
						cv2.rectangle(imgcont,(x,y),(x+w,y+h),(0,255,0),2)
						rects.append([x, y, x + w, y + h])
	objects = ct.update(rects)

	# loop over the tracked objects
	for (objectID, centroid) in objects.items():
		# draw both the ID of the object and the centroid of the
		# object on the output frame
		text = "ID {}".format(objectID)
		cv2.putText(imgcont, text, (centroid[0] - 10, centroid[1] - 10),
			cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
		cv2.circle(imgcont, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

	cv2.imshow('teste',imgcont)
	print("Peoples in env: ", len(rects))
	rects = []
	key = cv2.waitKey(1) & 0xFF
	if key == 'q':
		break
cap.realease()
cv2.destroyAllWindows()
