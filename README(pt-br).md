# Smart Light


## Introdução

O projeto SmartLight realiza uma iluminação inteligente do ambiente de acordo com o número de pessoas que nele se encontram. Foi criado com base na possiblidade de gerar economia de energia elétrica, visto que não é necessário que uma lâmpada fique em sua potência máxima quando ninguém está perto.


## Hardware Utilizado

- Essa aplicação foi executada na plataforma de desenvolvimento DragonBoard410c. A imagem do sistema operacional é a linaro-buster-alip-dragonboard-410c-359.img.gz e pode ser obtida neste [link](https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/)

- Versão do kernel: 

- Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux 

- Cabe ressaltar que o sistema utiliza um uma memória externa para expandir a capacidade do rootfs e fornecer uma região de swap. A configuração desse mecanismo é a seguinte: de 4GB de swap e o restante dedicado para o rootfs. 

- A luminária, de modelo convencional e lâmpadas de LED, ligada à rede elétrica 220V

- Um shield Mezzanine 96Boards Sensors Board, para interface entre o circuito de controle e a Dragonboard. O Mezaninne conta com um Arduino para interface, facilitando a integração

- Um circuito de controle para acionamento da luminária, possibilitando a variação de intensidade da iluminação

- Para a demonstração em ambiente interno no demoroom do IoT Reference Center, uma caixa de som bluetooth para a reprodução da mensagem de boas vindas



### Equipamento

Os equipamentos necessários para a montagem da aplicação são:
* DragonBoard 410c;
* Fonte de 12V;
* Câmera USB;
* Mezzanine Sensors Board;
* Monitor com entrada HDMI;
* Cartão micro SD (16GB);
* Teclado USB;
* Circuito de controle;
* Luminária LED.
  

## Frameworks, bibliotecas e softwares utilizados

Execute o tutorial deste [link](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(pt-br).md) para criar uma região de momória Swap no cartão SD.

No terminal da DragonBoard, execute os comandos de instalação das bibliotecas:

```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
Sudo apt-get install python3-dev
sudo apt-get install python3-opencv
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module
sudo apt install python3-pip
sudo –H python3 –m pip install scipy
```

## Funcionamento 

**1. Circuito elétrico**

A seguir, será detalhado o funcionamento do circuito de controle. O circuito tem dois objetivos:
* Marcar a frequência de acionamento proveniente da rede, utilizada como dado de entrada para o Arduino do Mezzanine.
* Interface entre o controle do software e a luminária, acionando os disparos para controle de potência

 <div align="center">
    <figure>
        <img src="/images/1.PNG">
        <figcaption>Zero Crossing Detector</figcaption>
    </figure>
</div>


Primeiramente, a rede de 127VAC passa por um circuito de detecção de passagem por 0 para que o controle de potência possa ser feito 

O circuito transforma a onda alternada em pulsante, que polariza o led e aciona o foto transistor. O resistor R2 faz o papel de resistor de pull-up, ou seja, sempre que o led do foto transistor está desligado (rede em 0VAC) a GPIO input Arduino estará energizada com 5VDC, quando um pulso ocorre e está com a tensão acima de 0VAC, o foto transistor conduz e a GPIO input Arduino vai para nível lógico 0. Desta forma, é possível identificar quando a tensão senoidal proveniente da rede passa do semicírculo positivo para o negativo ou o contrário. 

A segunda parte do circuito, consiste no controle de luminosidade a partir do arduino. Para controlar a luminosidade, é necessário diminuir a potência sobre a lâmpada, para isso foi utilizado um triac com um snubber. 


 <div align="center">
    <figure>
        <img src="/images/2.PNG">
        <figcaption>Controle de Potência AC</figcaption>
    </figure>
</div>


A saída do arduino é ligada ao foto triac, que serve para isolar a saída do microcontrolador da rede. O foto triac U3 está ligado ao gate do triac U2, este por sua vez é responsável por permitir ou não que a lâmpada conduza. Conforme o ângulo de disparo encima do triac U2, a luminosidade da lâmpada varia. Neste caso, o arduino consegue sincronizar os disparos com a rede através do circuito de detecção de passagem por zero. 

 <div align="center">
    <figure>
        <img src="/images/3.PNG">
        <figcaption>Triac output</figcaption>
    </figure>
</div>

**2. Ciclo de funcionamento (Software)**

O principal método para que o Smartlight funcione é o "cv2.createBackgroundSubtractorMOG2()". Ele é responsável por fazer a subtração do plano de fundo da imagem. Após esse processo, a imagem resultante é apenas o contorno de uma pessoa destacado em fundo preto.
Em seguida, é realizada a detecção da pessoa na imagem com o métovo "cv2.findContours()". Uma vez detectada a posição, o acompanhamento de deslocamento é feito no vídeo, com uma técnica de tracking.

Quando uma ou mais pessoas são detectadas, o software envia um comando serial para a placa Mezzanine, solicitando o acionamento do circuito de controle com certa intensidade. O circuito acende a luminária e aguarda um novo comando.
O software acompanha o deslocamento das pessoas com o trakking, e caso elas saiam da área iluminada, envia um comando para o circuito desligar a luminária.

## Execução do projeto

### Criação do circuito de controle

Para a execução, é preciso criar a placa do circuito de controle. O esquemático do circuito está representado na figura a seguir:

 <div align="center">
    <figure>
        <img src="/images/Cirquito Smart Light.JPG">
        <figcaption>Circuito de Controle</figcaption>
    </figure>
</div>

A imagem de impressão utilizada:

 <div align="center">
    <figure>
        <img src="/images/Capturar.JPG">
        <figcaption>PCB</figcaption>
    </figure>
</div>

Uma representação da placa pronta:

 <div align="center">
    <figure>
        <img src="/images/Smart Light.JPG">
        <figcaption>Circuito de Controle</figcaption>
    </figure>
</div>

O circuito foi desenhado como um shield para a Mezzanine. O Encaixe é feito nos pinos de Arduíno.

### Instalação do software

No terminal da placa, clone o repositório do projeto:

``` sh
git clone https://gitlab.com/qualcomm-iot-reference-center/smartlight-v2.git
cd smartlight-v2

``` 

Para executar o código, rode o comando:

``` sh

python3 main_mog.py

```

A aplicação irá iniciar. Na tela será exibida a contagem de pessoas. A lâmpada deve acionar com intensidade baixa caso uma pessoa seja detectada pela câmera, e com intesidade alta conforme o número de pessoas aumente.